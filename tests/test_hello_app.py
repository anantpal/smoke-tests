import os
import requests

env = os.getenv('ENV_NAME', 'dev')


def test_greeting_without_name():
    response = requests.get(f'http://hello-app.{env}.akpal.in/hello', timeout=10)
    assert response.status_code == 200
    assert response.text == 'Hello!!! John'


def test_greeting_with_name():
    response = requests.get(f'http://hello-app.{env}.akpal.in/hello?name=pal', timeout=10)
    assert response.status_code == 200
    assert response.text == 'Hello!!! pal'
